" for comments
" start searching straight away
set incsearch
" I don't think we need this
"set ignorecase
" smartcase should ignore case unless you use a capital
set smartcase
" set g as the default scope for searches
"set gdefault
syntax enable
set tabstop=4
set shiftwidth=4
set softtabstop=4
set expandtab
set autoindent
colorscheme evening
autocmd BufRead *.py set smartindent cinwords=if,elif,else,for,while,try,except,finally,def,class
runtime! ftplugin/man.vim
set nrformats-=octal
filetype plugin indent on
syntax on
