## Script (Python) "c_edit_prc_a"
##parameters=delete='', cancel='', clear=''
##title=Edit an instance

from Products.CMFWebfactory.utils import isEmailValid

REQUEST = context.REQUEST
RESPONSE = REQUEST.RESPONSE
properties = REQUEST.form.copy()
errors = {}
if properties.has_key('-C'):
    del(properties['-C'])

# set up default values
values = {}
REQUEST.set('values', {} )
REQUEST.set('errors', {} )

if len( properties.keys() ) == 0:
    # processed values - always an id 
    values['came_from'] = REQUEST.get('HTTP_REFERER')
    REQUEST.set('values', values)
    return

location = properties.get('came_from')

if properties.has_key('cancel'):
    RESPONSE.redirect(location)

elif properties.has_key('workflow_action'):
    if properties['workflow_action'] == 'Delete':
        location = context.absolute_url()+"/f_provider_instance_delete_form?came_from=%s" %location
    else:
        if properties.has_key('description'):

            # if there is any submitted data
            for k in ('title'
                      ,'description'
                      ,'instance_select'
                      ,'instance_radio'
                      ):
               if not properties.get(k,'').strip():
                  errors[k] = 'You must provide a value '

            # return the error messages
            # set them here as empty values required for redirect to work
            REQUEST.set('errors', errors )
            REQUEST.set('values', properties )

            if len(errors.keys()):
               return

            title = REQUEST.get('title')
            context.edit(title=properties['title']
                        ,description=properties['description']
                        ,instance_select=properties['instance_select']
                        ,instance_radio=properties['instance_radio']
                         )

        if properties['workflow_action'] != 'Save':
            context.portal_workflow.doActionFor(context,action=properties['workflow_action'].lower())

context.REQUEST.RESPONSE.redirect(location) 

