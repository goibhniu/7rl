## Script (Python) "c_add_prc_a"
##parameters= cancel='', clear=''
##title=Add an instance

from Products.CMFWebfactory.utils import isEmailValid
from Products.CMFWebfactory.utils import generateIdFor

REQUEST = context.REQUEST
RESPONSE = REQUEST.RESPONSE
properties = REQUEST.form.copy()
errors = {}
if properties.has_key('-C'):
    del(properties['-C'])

# set up default values
REQUEST.set('values', {} )
REQUEST.set('errors', {} )

location = "/return_to_here"

if properties.has_key('cancel'):
    #location = properties['cancel_location']
    RESPONSE.redirect(location)

elif len( properties.keys() ) == 0:
    # processed values - always an id 
    return

else:
    # if any submitted data
    for k in ('title'
              ,'description'
              ,'instance_select'
              ,'instance_radio'
              ):
       if not properties.get(k,'').strip():
          errors[k] = 'You must provide a value '

    # return the error messages
    # set them here as empty values required for redirect to work
    REQUEST.set('errors', errors )
    REQUEST.set('values', properties )

    if len(errors.keys()):
       return

    title = REQUEST.get('title')
    context.portal_user_contributions.addContent(
                area=context
                ,type='Instance'
                ,title=properties['title']
                ,description=properties['description']
                ,instance_select=properties['instance_select']
                ,instance_radio=properties['instance_radio']
                )

context.REQUEST.RESPONSE.redirect(location) 

