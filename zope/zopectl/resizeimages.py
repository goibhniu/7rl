site = app.broadway
pc = site.portal_catalog
rs = pc.unrestrictedSearchResults(portal_type='ThumbImage')

for r in rs:
    ob = r.getObject()
    config = ob.findConfig()
    width = int(config.get('width', 0))
    medium_width = int(config.get('medium_width', 0))
    large_width = int(config.get('large_width', 0))
    huge_width = int(config.get('huge_width', 0))
    default_size = config.get('default_size', None)
    allow_raw = int(config.get('allow_raw', 0))
    ob._edit(small_width=width,
        medium_width=medium_width,
        large_width=large_width,
        huge_width=huge_width,
        configure_edit=True)
    print ob.getId() 

#transaction.commit()
