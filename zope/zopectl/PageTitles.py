import urllib, urllib2
from BeautifulSoup import BeautifulSoup
import codecs

pages = open("failed-titles.txt", "r").readlines()

results = codecs.open("fails.txt", "w", "utf-8")
for i in pages:
    page = urllib2.urlopen(i)
    #safeurl = urllib.quote(page.url.encode("utf-8"))
    safeurl = page.url
    if page.msg == "OK":
        try:
            soup = BeautifulSoup(page.read())
            try:
                title = soup.head.title.string
            except AttributeError:
                title = "ERROR: NO TITLE"
        except:
            title = [i for i in page.readlines() if "<title>" in i]
        res = "'%s','%s'\n"%(safeurl, title)
        results.write(res)
        print res
    else:
        results.write("'%s','%s'\n" %(safeurl, 'error'))

results.close()
