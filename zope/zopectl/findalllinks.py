from BeautifulSoup import BeautifulSoup

site = app.fpa
pc = site.portal_catalog
qry = {}
qry['portal_type'] = 'Document'
rs = pc.unrestrictedSearchResults(portal_type='Document')

results = open("search-results.csv", "w")

bag = set()
for r in rs:
    ob = r.getObject()
    soup = BeautifulSoup(ob.CookedBody())
    #print ob.CookedBody()
    try:
        links = [i['href'] for i in soup.findAll('a') if 'cfm' in i['href']]
    except:
        pass
    #links = [i['href'] for i in soup.findAll('a') ]
    for i in links:
        bag.add(i)
    #if links:
    #    results.write("'"+ob.absolute_url()+"'")
    #    results.write(",'"+"','".join(links)+"'")
    #    results.write("\n")
    
results.write("\n".join(bag))
results.close()
#transaction.commit()
