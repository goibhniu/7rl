from BeautifulSoup import BeautifulSoup
from Acquisition import aq_inner, aq_parent, aq_base, aq_chain, aq_get
from Products.CMFCore.utils import getToolByName
import transaction

portal = app.OSHA
membership_tool = getToolByName(portal, 'portal_membership')
from AccessControl.SecurityManagement import newSecurityManager, getSecurityManager
user = app.acl_users.getUser('admin').__of__(portal.acl_users)
newSecurityManager(None, user)


pc = portal.portal_catalog
qry = {}
qry['portal_type'] = 'Document'
rs = pc.unrestrictedSearchResults(portal_type='Document')

replacelog = open("searchreplace.log", "w")
errorlog = open("searchreplace-error.log","w")

mapping = {}
maplist = [i.split(",") for i in open('matches.csv','r').readlines()]
for i in maplist:
    mapping[i[0]] = i[1]

for r in rs:
    try:
        ob = r.getObject()
        body = ob.CookedBody()
        soup = BeautifulSoup(body)
        changed = False
        url = ob.absolute_url()
        for i in soup.findAll('a'):
            if i.has_key('href'):
                link = i['href']
                if "cfm" in link:
                    if link in mapping.keys():
                        changed = True
                        i['href'] = mapping[link][:-1]
        if changed:
            replacelog.write("'%s','%s','%s\n'"%(url,body,soup))
            #import pdb; pdb.set_trace()
            ob.text = str(soup)
            ob.cooked_text = str(soup)
            print url
            transaction.commit()
    except Exception, error:
        import pdb; pdb.set_trace()
        errorlog.write("'%s','%s\n'"%(url,error))

replacelog.close()
errorlog.close()
#transaction.commit()
