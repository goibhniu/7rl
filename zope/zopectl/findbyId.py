from BeautifulSoup import BeautifulSoup
from Acquisition import aq_inner, aq_parent, aq_base, aq_chain, aq_get
from Products.CMFCore.utils import getToolByName
membership_tool = getToolByName(app.fpa, 'portal_membership')
from AccessControl.SecurityManagement import newSecurityManager, getSecurityManager
user = app.acl_users.getUser('admin').__of__(app.fpa.acl_users)
newSecurityManager(None, user)

site = app.fpa
pc = site.portal_catalog
qry = {}
idfile = open("ids.txt","r")

#ids = [i[:-1] for i in idfile.readlines()]
#qry['getId'] = ids
qry['portal_type'] = ['FAQ','GenericPage','SectionHomePage']
rs = pc.unrestrictedSearchResults(qry)

results = open("urls-ids-all-titles.csv", "w")

for r in rs:
    ob = r.getObject()
    results.write("'%s','%s', '%s'\n" %(ob.absolute_url(),r.getId,r.LongTitle))
    #results.write(r.getId+"\n")
    
results.close()
#transaction.commit()
