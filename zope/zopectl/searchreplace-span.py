from BeautifulSoup import BeautifulSoup
from Acquisition import aq_inner, aq_parent, aq_base, aq_chain, aq_get
from Products.CMFCore.utils import getToolByName
membership_tool = getToolByName(app.fpa, 'portal_membership')
from AccessControl.SecurityManagement import newSecurityManager, getSecurityManager
user = app.acl_users.getUser('admin').__of__(app.fpa.acl_users)
newSecurityManager(None, user)

import transaction
site = app.fpa
pc = site.portal_catalog
qry = {}
qry['portal_type'] = 'Document'
rs = pc.unrestrictedSearchResults(qry)

replacelog = open("searchreplace-span.log", "w")
errorlog = open("searchreplace-span-error.log","w")

for r in rs:
    try:
        ob = r.getObject()
        body = ob.CookedBody()
        soup = BeautifulSoup(body)
        changed = False
        url = ob.absolute_url()
        for i in soup.findAll('span'):
            if i.has_key("class"):
                if i['class'] == "Apple-style-span":
                    contents = 1 and i.first() or i.string
                    i.replaceWith(contents)
                    changed = True
        if changed:
            replacelog.write("'%s','%s','%s\n'"%(url,body,soup))
            ob.text = str(soup)
            ob.cooked_text = str(soup)
            print url
            transaction.commit()
    except Exception, error:
        errorlog.write("'%s','%s\n'"%(url,error))
        #import pdb; pdb.set_trace()

replacelog.close()
errorlog.close()
#transaction.commit()
