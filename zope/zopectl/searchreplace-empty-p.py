from BeautifulSoup import BeautifulSoup
from Acquisition import aq_inner, aq_parent, aq_base, aq_chain, aq_get
from Products.CMFCore.utils import getToolByName
membership_tool = getToolByName(app.fpa, 'portal_membership')
from AccessControl.SecurityManagement import newSecurityManager, getSecurityManager
user = app.acl_users.getUser('admin').__of__(app.fpa.acl_users)
newSecurityManager(None, user)

import transaction
site = app.fpa
pc = site.portal_catalog
qry = {}
qry['portal_type'] = 'Document'
rs = pc.unrestrictedSearchResults(qry)

replacelog = open("searchreplace-p.log", "w")
errorlog = open("searchreplace-p-error.log","w")

for r in rs:
    try:
        ob = r.getObject()
        body = ob.CookedBody()
        soup = BeautifulSoup(body)
        changed = False
        url = ob.absolute_url()
        for i in soup.findAll('p'):
            if u"&nbsp;'" == i.string:
                if link in mapping.keys():
                    changed = True
                    i['href'] = mapping[link][:-1]
        if changed:
            replacelog.write("'%s','%s','%s\n'"%(url,body,soup))
            #import pdb; pdb.set_trace()
            ob.text = str(soup)
            ob.cooked_text = str(soup)
            print url
            transaction.commit()
    except Exception, error:
        import pdb; pdb.set_trace()
        errorlog.write("'%s','%s\n'"%(url,error))

replacelog.close()
errorlog.close()
#transaction.commit()
