import sys
import os
import time

dr = os.path.dirname(os.path.abspath(__file__))
jn = os.path.join

def system(cmd):
    print cmd
    os.system(cmd)

def adhoc(path, extra=None):
    dirname, filename = os.path.split(path)
    filename = filename.split(".py")[0]
    sys.path.insert(0, dirname)
    if extra:
        sys.path.insert(0, extra)
    __import__(filename)
    del sys.path[0]
    if extra:
        del sys.path[0]

def python():
    """ Returns the current python being used """
    return sys.executable

def recover(zope, src, dest):
    print "Starting backup"
    print "Note: Ensure that zope is stopped"
    print time.asctime()

    repozo = jn(zope, "bin/repozo.py")
    fstest = jn(zope, "bin/fstest.py")
    zope = jn(zope, "lib/python")

    # test it a bit...
    assert os.path.exists(zope)
    assert os.path.exists(repozo)
    assert os.path.exists(fstest)
    assert os.path.exists(dest)
    assert os.path.exists(src)  
    print "Adding path: %s" % zope
    adhoc(repozo, zope)

    print "Looking for files in: %s" % src
    files = os.listdir(src)
    print "Found %x files" % len(files)
    for file in files:
        full = jn(src, file)
        if os.path.isdir(full):
            dct = {
                "python":python(),
                "repozo":repozo,
                "fstest":fstest,
                # so its from /tmp/backup/Data/some.backup.fsz to /tmp/var/Data.fs
                "dest":"%s/%s.fs" % (dest, file),
                "zope":zope,
                "full":full,
                "export":"export PYTHONPATH=%s:$PYTHONPATH" % zope

            }
            
            print "Recovering up %(full)s to %(dest)s" % dct
            system("export PYTHONPATH=%(zope)s:$PYTHONPATH; %(python)s %(repozo)s -Rv -r %(full)s -o %(dest)s " % dct)
            print "Checking integrity %(dest)s" % dct
            system("export PYTHONPATH=%(zope)s:$PYTHONPATH; %(python)s %(fstest)s %(dest)s" % dct)
            print "...completed"
    else:
        print "* no directories found, check your paths and that you wanted to run a recovery, you must point at the directory above the files"

    print time.asctime()
    print "...all completed"

def backup(zope, src, dest):
    print "Starting backup"
    print time.asctime()
    
    repozo = jn(zope, "bin/repozo.py")
    fstest = jn(zope, "bin/fstest.py")
    zope = jn(zope, "lib/python")
    
    # test it a bit...
    print "Checking scripts exist"
    assert os.path.exists(zope)
    assert os.path.exists(repozo)
    assert os.path.exists(fstest)        
    assert os.path.exists(src)
    assert os.path.exists(dest)
    print "Adding path: %s" % zope
    
    adhoc(repozo, zope)
    adhoc(fstest, zope)
    
    print "Looking in: %s" % src
    for file in os.listdir(src):
        if file.endswith(".fs"):
            full, short = jn(src, file), file.split(".fs")[0]
            dct = {
                "python":python(),
                "repozo":repozo,
                "fstest":fstest,
                "dest":jn(dest, short),
                "zope":zope,
                "file":full,
                "export":"export PYTHONPATH=%s:$PYTHONPATH" % zope
            }
            
            print "Backing up %(file)s to %(dest)s" % dct
                
            if not os.path.exists(dct["dest"]):
                print "Creating %(dest)s" % dct
                os.mkdir(dct["dest"])
            
            system("%(export)s; %(python)s %(repozo)s -BzQ -r %(dest)s -f %(file)s " % dct)
            print "Checking integrity %(file)s" % dct
            system("%(export)s; %(python)s %(fstest)s %(file)s" % dct)
                        
            print "...completed"
    else:
        print "* no files found, check your paths and that you wanted to run a backup"
    print time.asctime()
    print "...all completed"



usage = """Backup and recovery script for a zope site. Options (see notes):

python backup.py backup [location to zope] [location of the var directory] [location of the backup directory] 
python backup.py recover [location to zope] [location of the backup direcory] [location of the var directory]

For repozo, if you give just the path to zope, we'll assume repozo is in
the zope/bin/repozo.py and the libs in the zope/lib/python directories, it
will then try it for you, if you get an import error... those assumptions
didn't work (normally).

Be sure to the use the same python that repozo uses and the script
will automatically add it in so you don't need to worry about the wrong path
"""

def main():
    """ Generate suitable backup scripts for the server """
    if len(sys.argv) < 3:
        print usage
    cmd = sys.argv[1]
    if cmd == "backup":
        print "Starting backup..."
        backup(*sys.argv[2:])
    elif cmd == "recover":
        print "Starting recovery..."
        recover(*sys.argv[2:])
    else:
        print usage
if __name__=='__main__':
    main()
