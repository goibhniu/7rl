#!/bin/sh

echo "from Acquisition import aq_inner, aq_parent, aq_base, aq_chain, aq_get"
echo "from Products.CMFCore.utils import getToolByName"
echo "membership_tool = getToolByName(app.$1, 'portal_membership')"
echo "from AccessControl.SecurityManagement import newSecurityManager, getSecurityManager"
echo "user = app.acl_users.getUser('admin').__of__(app.$1.acl_users)"
echo "newSecurityManager(None, user)"
echo "getSecurityManager().getUser()"
echo "$1 = app.$1"
echo "pc = $1.portal_catalog"
echo "pm = $1.portal_membership"
