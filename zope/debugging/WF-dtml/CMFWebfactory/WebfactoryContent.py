import sys
import copy
import string
import urllib
from cgi import escape
from DateTime import DateTime

from zLOG import LOG, INFO, WARNING, DEBUG, ERROR
from Acquisition import aq_base, aq_parent, aq_inner
from Acquisition import Implicit
from OFS.PropertyManager import PropertyManager
from ZPublisher import BeforeTraverse
from AccessControl import ClassSecurityInfo
from AccessControl.unauthorized import Unauthorized

from Products.CMFCore.PortalContent import PortalContent
from Products.CMFCore.utils import _checkPermission
from Products.CMFCore.permissions import AddPortalContent
from Products.CMFCore.permissions import View
from Products.CMFCore.utils import getToolByName,_getAuthenticatedUser
from Products.CMFCore.permissions import ModifyPortalContent
from Products.CMFCore.WorkflowCore import WorkflowAction
from exceptions import WebfactoryError
from Products.CMFDefault.DublinCore import DefaultDublinCoreImpl
from Products.CMFDefault.utils import tuplize
from Products.CMFWebfactory.utils import callView, pp
from Products.CMFWebfactory.utils import _getViewFor
from Products.CMFWebfactory.utils import godPowerOn, godPowerOff
from Products.CMFWebfactory.utils import addQueryParam
from exceptions import UniqueIdError
from zExceptions import Unauthorized
from OFS.ObjectManager import BeforeDeleteException

from zope.interface import implements
from interfaces import IContentArea
from interfaces import ICompositeContent
from interfaces import IWebfactoryPage
from interfaces import IWebfactorySiteRoot
from Products.CMFCore.interfaces import IContentish

from warnings import warn

try:
   from zope.app.content_types import add_files
   ZOPE_29 = True
except ImportError:
   ZOPE_29 = False

_marker=[]

class WebfactoryContentModifier(DefaultDublinCoreImpl, PropertyManager):
    """
        Mix-in class to add the methods required by webfactory content
        to the CMFDefault content types

        Tricky inheritance issue means that webfactorycontent cant be used
        Otherwise methods from SimpleItem override those of the CMFDefault
        content type

        Provides a means for us to dynamically select our view
        dependent on context.

        Assumes it represents a component; key methods are overriden
        in WebfactoryPage where necessary.
    """

    hide_from_search = 1
    notify_members = ()

    security = ClassSecurityInfo()
    security.declareObjectProtected(View)

    def __init__(self, id, title=''):
        DefaultDublinCoreImpl.__init__(self, title)
        self.id = id
        self.position = 1
        self.notify_members = []
        self.subtype = ''

    # properties:
    # - someone adds a property to a type
    # - someone
    def valid_property_id(self, id):
        """Overrides that in PropertyManager so that we don't look
        up the id in the Type's schema"""
        if not id or id[:1]=='_' or (id[:3]=='aq_') \
               or (' ' in id) or id in aq_base(self).objectIds() \
               or escape(id) != id:
            try:
                aq_base(self).__getattr__(id, 0)
                return 0
            except AttributeError:
                return 1
        return 1

    def __recurse(self, name, *args):
        """
            Recurse in both normal and opaque subobjects.
        """
        # if we are using zope29 then we dont want to recurse
        # through subobjects as this is done automatically
        if ZOPE_29:
            values = ()
        else:
            values = self.objectValues()
        opaque_values = self.opaqueValues()
        for subobjects in values, opaque_values:
            for ob in subobjects:
                s = getattr(ob, '_p_changed', 0)
                if hasattr(aq_base(ob), name):
                    getattr(ob, name)(*args)
                if s is None: ob._p_deactivate()

    def getProperty(self, attr, default=_marker):
        """Get the property 'id', returning the optional second
        argument or None if no such property is found.

        Properties may be found either on the object or may be
        deduced and seeded with default values from the schema
        propertysheet defined on its Type.
        """
        if self.hasProperty(attr):
            value = PropertyManager.getProperty(self, attr, default)
        else:
            try:
                value = self.getSchemaProperty(attr)
                ti = self.getTypeInfo()
                md = ti._schema.propdict()[attr]
                self._setProperty(md["id"], md.get('value', value), md['type'])
            except AttributeError:
                value = default
        if value is _marker:
            raise AttributeError
        return value

    def getSchemaProperty(self, attr):
        """Get the property 'id' from our Type's 'schema' propertysheet.

        Raises an AttributeError if it does not exist there."""
        ti = self.getTypeInfo()
        if ti:
            if not ti._schema.hasProperty(attr):
                raise AttributeError
            return ti._schema.getProperty(attr)
        else:
            raise AttributeError

    def __call__(self, action=_marker, actions=_marker, template=_marker):
        '''
        Invokes the default view, with caveats
        '''
        ti = self.getTypeInfo()
        if action is not _marker or actions is not _marker or template is not _marker:
            warn("Type %s at %s got deprecated parameters (%s, %s, %s)" % ( ti and ti.getId(), '/'.join( self.getPhysicalPath()), action, actions, template ), DeprecationWarning)

        method_id = ti and ti.queryMethodID('(Default)', context=self)
        if method_id and method_id != '(Default)':
            template = getattr(self, method_id)
        else:
            # fix up default value
            if action is _marker:
                action = 'view'
            template = _getViewFor(self, view=action, actions=actions, template=template)

        import zLOG

        template_clean = str(template).replace("&lt;","<").replace("&gt;",">")
        zLOG.LOG('WFDebug', severity=0, summary='', detail='WebfactoryContent template:\n%s \n\n%s \n\n%s \n\n%s' % (method_id, self, self.getPhysicalPath(), template_clean))

        if getattr(aq_base(template), 'isDocTemp', 0):
           return template(self, self.REQUEST, self.REQUEST['RESPONSE'])
        else:
           return template(self.REQUEST)

    def forum_position(self):
        return None

    def getAcquiredHideFromSearch(self):
        """Returns true if any parent is to be hidden from search
        """
        ob = self
        hide_from_search = 0

        while hide_from_search==0:
            hide_from_search = getattr(aq_base(ob), 'hide_from_search', 0)
            if hide_from_search:
                break
            ob = aq_parent(ob)
            if ob is None or IWebfactorySiteRoot.isImplementedBy(ob):
                break
        return hide_from_search

    # this is all old
    # only w_edit_mode_wrapper uses getTemplateName

    def hasAction(self, action):
        """Returns True if this page type has the specified action.
        """
        try:
            self.getTemplateName(action=action, add_default=False)
        except Unauthorized:
            return False
        return True

    def getTemplateName(self, action='', actions=_marker, add_default=True):
        return _getViewFor(self, view_id=action, actions=actions)

    def __before_publishing_traverse__(self, arg1, arg2=None):
        """ Pre-traversal hook.
        """
        # Duplicated in from DynamicType
        # XXX hack around a bug(?) in BeforeTraverse.MultiHook
        REQUEST = arg2 or arg1

        if REQUEST['REQUEST_METHOD'] not in ('GET', 'POST'):
            return

        stack = REQUEST['TraversalRequestNameStack']
        key = stack and stack[-1] or '(Raw)'
        ti = self.getTypeInfo()
        method_id = ti and ti.queryMethodID(key, context=self)
        if method_id:
            if key != '(Raw)':
                stack.pop()
            if method_id != '(Default)':
                stack.append(method_id)
            REQUEST._hacked_path = 1


    def __bobo_traverse__(self, REQUEST, name):
        """If the name we're looking for starts with a magic token, we look for it
        using the Uid tool
        """
        name = urllib.unquote(name)
        if name.startswith("@"):
            # check for unique id handler tool
            handler = getToolByName(self, 'portal_uidhandler', None)
            if handler is not None:
                # check for remote uid info on object
                uid = name[1:]
                sc = godPowerOn(self)
                obj = handler.unrestrictedQueryObject(uid, None)
                godPowerOff(sc)
                if obj is not None:
                    return obj.__of__(self)
        return getattr(self, name)

    __fallback_traverse__ = __bobo_traverse__

    def manage_afterAdd(self, item, container):
        """Register the item with the uid handler
        """
        try:
            anno_tool = getToolByName(item, 'portal_uidannotation')
            if anno_tool.remove_on_clone:
                raise WebfactoryError('Portal UID Annotation Tool must have "remove after clone" unticked')
        except AttributeError, e:
            pass # We can ignore looking up the tool failures
        # customised copy of CMFCatalogAware.manage_afterAdd
        self.indexObject()
        if not ZOPE_29:
            self.__recurse('manage_afterAdd', item, container)
        # end customised copy
        pu = getToolByName(self, 'portal_uidhandler', None)
        if pu is not None: pu.register( self )

        # rewrite tool support
        pu = getToolByName(item, 'portal_url_rewrite', None)
        if pu is not None: pu.manage_notifyAdded( self )

    def manage_afterClone(self, item):
        """
            Add self to the workflow.
            (Called when the object is cloned.)
        """
        # copy of cmfcatalogaware so that we get to use our own __recurse
        self.notifyWorkflowCreated()
        if not ZOPE_29:
            self.__recurse('manage_afterClone', item)

        pu = getToolByName(self, 'portal_uidhandler', None)
        if pu is not None:
            try:
               pu.unregister(self)
            except UniqueIdError:
               # no uid to unregister, but that's okay
               pass
            pu.register(self)

        # Make sure owner local role is set after pasting
        # The standard Zope mechanisms take care of executable ownership
        current_user = _getAuthenticatedUser(self)
        if current_user is not None:
            local_role_holders = [x[0] for x in self.get_local_roles()]
            self.manage_delLocalRoles(local_role_holders)
            self.manage_setLocalRoles(current_user.getId(), ['Owner'])

    def manage_beforeDelete(self, item, container):
        """Store the current uid in case we're moving the item."""
        # customised from CMFCatalogAware to use our own __recurse
        if not ZOPE_29:
            self.__recurse('manage_beforeDelete', item, container)
        self.unindexObject()
        # Objects that are created in the temporary folder don't seem to get a cmf_uid
        if hasattr(aq_base(aq_inner(self)), 'cmf_uid'):
            self._orig_uid = self.cmf_uid

        # rewrite tool support
        pu = getToolByName(self, 'portal_url_rewrite', None)
        if pu is not None:
            pu.manage_notifyDeleted( self )

    def findConfigAndConfigure(self):
        self.configure(self.findConfig())

    ## Find the parental Configuration for a type
    def findConfig(self):
        # If we're in a content area, use its config
        # Otherwise, if we're directly in a page, use its config for
        # a named slot component with our name
        # Otherwise, something is not as it should be.
        configuration_source = aq_parent(aq_inner(self))
        return configuration_source.getConfiguration( component_type=self.portal_type, component_id=self.getId() )

    def configure(self, config):
        pass

    def listConfigureInfo(self):
        """Return a tuple of 'config_name', 'type', and 'description' dictionaries,
        for enumerating the possible configuration options.
        """
        return []

    def isComponent(self):
        # this is needed because diplodocus does not
        # have content areas
        if self.portal_type in ('RightContent','LeftContent'):
           return True
        else:
           return not IWebfactoryPage.isImplementedBy(self)

    def isContentArea(self):
        return IContentArea.isImplementedBy(self)

    def getContentArea(self):
        ob = self
        area = None
        while IContentish.isImplementedBy(ob):
           if IContentArea.isImplementedBy(ob):
              area = ob
              break
           ob = aq_parent(ob)
        return area

    def isCompositeComponent(self):
        return ICompositeContent.isImplementedBy(self)

    def getPage(self):
        ob = self
        page = None
        while IContentish.isImplementedBy(ob):
           if IWebfactoryPage.isImplementedBy(ob):
              page = ob
              break
           ob = aq_parent(ob)
        return page
    getWebfactoryPage = getContainer = getPage

    def isRepositoryContent(self):
        """Is the item in a repository
        """
        # if in portal_repository, always want to use a UID
        # to preserve the context
        return 'portal_repository' in self.getPhysicalPath()

        #return getattr(aq_base(self), '_wf_in_repository_', False)

    def getUid(self):
        """Return the uid for the object (or None if there isn't one)
        """
        uid_handler = getToolByName(self, 'portal_uidhandler', None)
        if uid_handler:
            uid = uid_handler.queryUid(self, None)
        else:
            uid = None
        return uid

    def getNewUID(self):
        uidhandler = getToolByName(self, 'portal_uidhandler', None)
        if uidhandler is not None:
            self.cmf_uid = None
            uidhandler.register(self)

    def getContainer(self):
        ob = self
        while IContentish.isImplementedBy( ob ):
           if IWebfactoryPage.isImplementedBy( ob ):
              break
           ob = aq_parent(ob)

        return ob

    def indexObject(self, **kw):
        PortalContent.indexObject(self)
        _notifyCompositeParent(self, **kw)

    def reindexObject(self, idxs=[], **kw):
        PortalContent.reindexObject(self, idxs)
        _notifyCompositeParent(self, idxs, **kw)

    def unindexObject(self, **kw):
        PortalContent.unindexObject(self)
        _notifyCompositeParent(self, **kw)

    # BBB: Things may try to call setComponent on components, from the
    # old model where they didn't already know.
    def setComponent(self):
        pass

    security.declarePublic( 'SubjectPath' )
    def SubjectPath( self ):
        "Extra subject stuff"
        # subjects are hierarchical and are stored as a list of paths
        # returns only paths that refer to valid nodes
        return getattr(self, 'subject_path', [])

    security.declareProtected(ModifyPortalContent, 'setSubject' )
    def setSubject( self, subject ):
        "Overrides DublinCore version, integrates with hierarchy tool"

        keywords = ""
        subject_words = ""
        subject_path = []

        paths = tuplize( 'subject', subject )
        try:
            if paths != ():
                ph = getToolByName(self, 'portal_hierarchy', None)
                if ph:
                    nodes = ph.getNodesAbovePaths( paths )
                    for node in nodes:
                        keywords = keywords + node.getTitle() + ", "

                    nodes = ph.getNodes( paths )
                    for node in nodes:
                        if node != ph:
                            subject_words = subject_words + node.getTitle() + ", "
                            subject_path.append( node.getPath() )

                    self.keywords = keywords
                    self.subject_words = subject_words
                    self.subject_path = subject_path
        except TypeError, e:
            self.log(text="We saw a TypeError %s in setSubject for item %s and subject %s " % (e, self, subject), log_level=ERROR)

        self.subject = paths

    def listCreators(self):
        """
        Make sure CMF creator handling takes into account
        our old, custom creator attribute
        """
        creators = DefaultDublinCoreImpl.listCreators(self)
        if hasattr(aq_base(self), 'creator'):
            # for old code that used custom creator method
            wf_creator = self.creator
            creators = (wf_creator,) + creators
            self.creators = creators
            delattr(self, 'creator')
        return creators

    def setCreator(self, creator=None):
        """
        old deprecated method
        add creator to the start of the list
        """
        if creator is None:
           mtool = getToolByName(self, 'portal_membership', None)
           creator = mtool and mtool.getAuthenticatedMember().getId()

        if creator:
           creators = self.creators
           self.creators = (creator,) + tuple([x for x in creators if x!=creator])

    # Custom Webfactory Metadata, from our old hacked version of DublinCore
    security.declarePublic( 'LongTitle' )
    def LongTitle( self ):
        "Not a Dublin Core element - resource name"
        return getattr(aq_base(self), 'long_title', '') and self.long_title or self.Title()

    security.declareProtected(ModifyPortalContent, 'setLongTitle')
    def setLongTitle( self, long_title ):
        "Not a Dublin Core element - resource name"
        self.long_title = long_title

    def sortable_title( self ):
        """
        normalised title for sorting in alphabetical order
        """
        if not self.title:
            return ""

        title = self.title.lower()
        # drop leading punctuation, etc
        allowed_start = string.lowercase + string.digits
        while title and title[0] not in allowed_start:
            title = title[1:]

        # normalize accents
        # title = accentNormalise(title)
        return title

    def SubType( self ):
        """
        type chosen by users, e.g. files can be minutes, agendas, etc...
        """
        return getattr(self.aq_base, 'subtype', '')

    def setSubType( self, subtype ):
        self.subtype = subtype

    __CEILING_DATE = DateTime( 2030, 0 ) # never expires

    #
    # Reviewer support
    #
    # all reviewer support code is handled in the
    # reviewer workflow so really should be removed
    # currently left for legacy reasons
    #

    # the user who has been asked to review this page
    security.declarePublic('ExpertReviewer')
    def ExpertReviewer(self):
        """
        """
        return getattr(self.aq_base, 'expert_reviewer', '')

    security.declareProtected(ModifyPortalContent, 'setExpertReviewer')
    def setExpertReviewer(self, expert_reviewer):
        "set it..."
        self.expert_reviewer = expert_reviewer

    # XXX We actually use ReviewDate as "date last reviewed", although this
    # isn't what it says below
    security.declarePublic('ReviewDate')
    def ReviewDate(self):
        """
            Dublin Core element - date resource requires review
        """
        review = self.review()
        return review and review.ISO() or None

    security.declarePublic('review')
    def review(self):
        """
            Dublin Core element - date resource requires review
              returned as DateTime.
        """
        date = getattr(self.aq_base, 'review_date', None)
        return date

    security.declareProtected(ModifyPortalContent, 'setReviewDate')
    def setReviewDate(self, review_date):
        """
            Dublin Core element - date resource requires review.
        """
        self.review_date = self._datify(review_date)

    # XXX We use *this* one as the date the resource requires review.
    security.declarePublic('NextReviewDate')
    def NextReviewDate(self):
        """
        """
        return hasattr(self, 'next_review_date') and self.next_review_date and self.next_review_date.ISO() or 'None'

    security.declarePublic('nextReview')
    def nextReview(self):
        """
        """
        date = getattr(self, 'next_review_date', None)
        return date is None and self.__CEILING_DATE or date

    security.declarePublic('suggestNextReviewDate')
    def suggestNextReviewDate(self):
        """
        """
        props = getToolByName(self, 'portal_properties')
        default_period = props.getProperty('default_review_period', 365)
        when = DateTime() + default_period
        return when

    security.declareProtected(ModifyPortalContent, 'setNextReviewDate')
    def setNextReviewDate(self, next_review_date=None):
        """Set the next review date.

        If not specified, use the automatically suggested next review date.
        """
        if not next_review_date:
            next_review_date = self.suggestNextReviewDate()
        self.next_review_date = self._datify(next_review_date)

    #
    #
    # end of reviewer support
    #
    #

    def apparent_url(self):
        """Return the apparent URL of the object, taking into account acquisition,
        and UIDs
        """
        pn = getToolByName(self, 'portal_navigation')
        return pn.apparent_url(self)

    def log(summary='', text='', log_level=INFO):
        LOG('WFCore.WebfactoryContent', log_level, summary, text)

    def getLayoutId(self):
        """Return an id suitable for applying CSS to on a component on a page.
        """
        if not aq_parent(aq_inner(self)).isContentArea():
            return self.getId()
        else:
            parent = aq_parent(aq_inner(self))
            ids = parent.contentIds()
            try:
                position = ids.index(self.getId()) + 1
            except ValueError:
                raise ValueError, 'Item %s is not in the list for %s' % ( '/'.join(self.getPhysicalPath()), '/'.join(parent.getPhysicalPath()) )
            return "%s-item-%d" % (parent.getId(), position)

    def getActionByChain(self, chain, obj=None, default=''):
        """Return an Action using the new Action Info method."""
        if obj == None:
            obj = self
        try:
            return obj.getTypeInfo().getActionInfo(action_chain=chain, object=obj)['url']
        except (ValueError, Unauthorized, AttributeError):
            return default

    def isComponentSlot(self):
        page = self.getPage()
        if not page:
            return False
        else:
            ti = page.getTypeInfo()
            return ti.isComponentSlot(self)

    def getTypeString(self):
        """Return the type id, or an empty string.
        """
        type_string = ""
        pt = self.getTypeInfo()
        if pt is not None:
            type_string = pt.getId()
        return type_string

    editMetadata = WorkflowAction(DefaultDublinCoreImpl.editMetadata)

    def addCacheBuster(self, url):
        cache_string=str(int(DateTime()))
        return addQueryParam(url, 'cs', cache_string)

    def latitude(self):
        return getattr(self,'_latitude', None)

    def longitude(self):
        return getattr(self,'_longitude', None)

    def hasCoordinates(self):
        return bool(self.longitude() and self.latitude())

def _notifyCompositeParent(ob, idxs=[], **kw):
    parent = aq_parent(ob)
    is_page = False
    while parent is not None:
       is_composite = ICompositeContent.isImplementedBy(parent)
       is_page = IWebfactoryPage.isImplementedBy(parent)
       if is_composite:
          parent.reindexCompositeAttributes(idxs=idxs, **kw)
          parent = aq_parent( parent )
       if (not is_composite) or is_page:
          break

class WebfactoryContent(WebfactoryContentModifier,
                        PortalContent):
    """
        Base class for Webfactory portal objects NOT derived from CMFDefault
    """

    pass
