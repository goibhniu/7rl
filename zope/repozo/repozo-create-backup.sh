#!/bin/sh

PYTHONBIN=/usr/local/zope/bin/python
REPOZOPATH=/usr/local/zope/bin/repozo.py
BACKUPPATH=/data/bfs/backup
DATAFILE=/data/bfs/zeo/var/Data.fs
PYTHONPATH=/usr/local/zope/lib/python

export PYTHONPATH

$PYTHONBIN $REPOZOPATH -BvzQ -r $BACKUPPATH -f $DATAFILE >> $BACKUPPATH/repozobackups.log 2>&1

