#!/bin/sh

PYTHONBIN=/usr/local/zope/bin/python
REPOZOPATH=/usr/local/zope/bin/repozo.py
BACKUPPATH=/data/bfs/backup
DATAFILE=/data/bfs/zeo/var/Data.fs
PYTHONPATH=/usr/local/zope/lib/python

export PYTHONPATH

$PYTHONBIN $REPOZOPATH -Rv -r $BACKUPPATH -o Restored-Data.fs >> repozobackups.log 2>&1

