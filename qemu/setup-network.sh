#!/bin/sh

# http://blog.falconindy.com/articles/build-a-virtual-army.html
vde_switch -tap tap0 -mod 660 -daemon -group kvm
ip addr add 10.0.2.1/24 dev tap0
ip link set dev tap0 up

sysctl -w net.ipv4.ip_forward=1
iptables -t nat -A POSTROUTING -s 10.0.2.0/24 -j MASQUERADE

echo "qemu-kvm -m 1024 -net nic,model=virtio -net vde -drive file=/home/goibhniu/OSimages/debian/squeeze/debian_squeeze_amd64_standard.qcow2 -loadvm current"

# Abandoning virtio
# http://kerneltrap.org/mailarchive/git-commits-head/2010/2/10/22994
# Error when doing a git clone
# "Invalid argument" after it calls fsync.
# host system is running 2.6.38.8,
# echo "qemu-system-x86_64 -m 1024 -net nic,model=virtio -net vde -drive file=/home/goibhniu/Downloads/debian_squeeze_amd64_standard.qcow2,if=virtio -virtfs local,path=/home/cillian/Apps/,security_model=passthrough,mount_tag=apz"


