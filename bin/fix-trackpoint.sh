#!/bin/sh

# Enable ThinkPoint scrolling
xinput set-int-prop "TPPS/2 IBM TrackPoint" "Evdev Wheel Emulation" 8 1
xinput set-int-prop "TPPS/2 IBM TrackPoint" "Evdev Wheel Emulation Button" 8 2
xinput set-int-prop "TPPS/2 IBM TrackPoint" "Evdev Wheel Emulation Timeout" 8 200
xinput set-int-prop "TPPS/2 IBM TrackPoint" "Evdev Wheel Emulation Axes" 8 6 7 4 5
xinput set-int-prop "Lite-On Technology Corp. ThinkPad USB Keyboard with TrackPoint" "Evdev Wheel Emulation" 8 1
xinput set-int-prop "Lite-On Technology Corp. ThinkPad USB Keyboard with TrackPoint" "Evdev Wheel Emulation Button" 8 2
xinput set-int-prop "Lite-On Technology Corp. ThinkPad USB Keyboard with TrackPoint" "Evdev Wheel Emulation Timeout" 8 200
xinput set-int-prop "Lite-On Technology Corp. ThinkPad USB Keyboard with TrackPoint" "Evdev Wheel Emulation Axes" 8 6 7 4 5
xinput set-int-prop 8 "Evdev Wheel Emulation" 8 1
xinput set-int-prop 8 "Evdev Wheel Emulation Button" 8 2
xinput set-int-prop 8 "Evdev Wheel Emulation Timeout" 8 200
xinput set-int-prop 8 "Evdev Wheel Emulation Axes" 8 6 7 4 5
xinput set-int-prop 9 "Evdev Wheel Emulation" 8 1
xinput set-int-prop 9 "Evdev Wheel Emulation Button" 8 2
xinput set-int-prop 9 "Evdev Wheel Emulation Timeout" 8 200
xinput set-int-prop 9 "Evdev Wheel Emulation Axes" 8 6 7 4 5
xinput set-int-prop 10 "Evdev Wheel Emulation" 8 1
xinput set-int-prop 10 "Evdev Wheel Emulation Button" 8 2
xinput set-int-prop 10 "Evdev Wheel Emulation Timeout" 8 200
xinput set-int-prop 10 "Evdev Wheel Emulation Axes" 8 6 7 4 5
xinput set-int-prop 12 "Evdev Wheel Emulation" 8 1
xinput set-int-prop 12 "Evdev Wheel Emulation Button" 8 2
xinput set-int-prop 12 "Evdev Wheel Emulation Timeout" 8 200
xinput set-int-prop 12 "Evdev Wheel Emulation Axes" 8 6 7 4 5
xinput set-int-prop 13 "Evdev Wheel Emulation" 8 1
xinput set-int-prop 13 "Evdev Wheel Emulation Button" 8 2
xinput set-int-prop 13 "Evdev Wheel Emulation Timeout" 8 200
xinput set-int-prop 13 "Evdev Wheel Emulation Axes" 8 6 7 4 5
xinput set-int-prop 14 "Evdev Wheel Emulation" 8 1
xinput set-int-prop 14 "Evdev Wheel Emulation Button" 8 2
xinput set-int-prop 14 "Evdev Wheel Emulation Timeout" 8 200
xinput set-int-prop 14 "Evdev Wheel Emulation Axes" 8 6 7 4 5
xinput set-int-prop 15 "Evdev Wheel Emulation" 8 1
xinput set-int-prop 15 "Evdev Wheel Emulation Button" 8 2
xinput set-int-prop 15 "Evdev Wheel Emulation Timeout" 8 200
xinput set-int-prop 15 "Evdev Wheel Emulation Axes" 8 6 7 4 5
xinput set-int-prop 16 "Evdev Wheel Emulation" 8 1
xinput set-int-prop 16 "Evdev Wheel Emulation Button" 8 2
xinput set-int-prop 16 "Evdev Wheel Emulation Timeout" 8 200
xinput set-int-prop 16 "Evdev Wheel Emulation Axes" 8 6 7 4 5
xinput set-int-prop 17 "Evdev Wheel Emulation" 8 1
xinput set-int-prop 17 "Evdev Wheel Emulation Button" 8 2
xinput set-int-prop 17 "Evdev Wheel Emulation Timeout" 8 200
xinput set-int-prop 17 "Evdev Wheel Emulation Axes" 8 6 7 4 5
xinput set-int-prop 11 "Evdev Wheel Emulation" 8 1
xinput set-int-prop 11 "Evdev Wheel Emulation Button" 8 2
xinput set-int-prop 11 "Evdev Wheel Emulation Timeout" 8 200
xinput set-int-prop 11 "Evdev Wheel Emulation Axes" 8 6 7 4 5
xinput set-int-prop 12 "Evdev Wheel Emulation" 8 1
xinput set-int-prop 12 "Evdev Wheel Emulation Button" 8 2
xinput set-int-prop 12 "Evdev Wheel Emulation Timeout" 8 200
xinput set-int-prop 12 "Evdev Wheel Emulation Axes" 8 6 7 4 5

# Set mouse/pointer velocity 
xset m 5 0

# Disable the caps lock
xmodmap ~/.xmodmap

kdialog --msgbox "Trackpoint configured!"
