import urllib2
from BeautifulSoup import BeautifulSoup

pages = open("urllist.txt", "r").readlines()
page = urllib2.urlopen("http://www.jamkit.com")

results = open("page-titles.txt", "w")
for i in pages:
    page = urllib2.urlopen(i)
    if page.msg == "OK":
        soup = BeautifulSoup(page.read())
        title = soup.head.title.string
        res = "'%s','%s'\n"%(page.url, title)
        results.write(res)
        print res
    else:
        results.write("'%s','%s'\n" %(page.url, 'error'))
results.close()
