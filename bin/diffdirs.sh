#!/bin/bash

# diff each file that is in both $1 and $2

for i in `ls $1`; 
do 
    if [ -e $2/$i ];  
    then echo "diff $1 $2" $i; 
    diff $1/$i $2/$i; 
    fi; 
done
