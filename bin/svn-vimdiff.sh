#!/bin/sh

# http://lojic.com/blog/2007/11/27/use-vimdiff-to-display-subversion-diffs/
/usr/bin/vimdiff ${6} ${7}
