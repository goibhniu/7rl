#!/bin/bash

if [ $# == 2 ]
then
    for i in `find . -path '*/.svn' -prune , -type f -a -name $1`;
    do
        TARGET1=`basename $i | sed 's/^w/c/g'`
        TARGET2=`basename $i`
        FULLTARGET1=`echo $2/$TARGET1 | sed 's/\/\//\//'`
        FULLTARGET2=`echo $2/$TARGET2 | sed 's/\/\//\//'`
        echo customise $i to:
        echo 1: $FULLTARGET1
        echo 2: $FULLTARGET2
        echo anything else skips this file
        read -e TARGETNO
        if [ $TARGETNO == "1" ]
        then
            cp -i $i $FULLTARGET1
        elif [ $TARGETNO == "2" ]
        then
            cp -i $i $FULLTARGET2
        else
            echo skipping $i
        fi
    done

else
    echo cust.sh find_expr target_dir
fi
