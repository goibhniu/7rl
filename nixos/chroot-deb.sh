#!/bin/sh

mount -o bind /proc /debian/proc
mount -o bind /dev/pts /debian/dev/pts
mount -o bind /home/cillian /debian/home/cillian 
mount -o bind /home/vg-home/backuppc /debian/home/backuppc

# was not able to install packages with this mounted
# mount -o bind /var/cache /mnt/debian/var/cache

# Copy the DNS settings and run sshd
cp /etc/resolv.conf /debian/etc/resolv.conf
chroot /debian/ /usr/sbin/sshd 
