{config, pkgs, ...}:
{
  require = [
    ./hardware-configuration.nix
  ];

  boot = {
    vesa = false;
    loader.grub = {
      enable = true;
      version = 2;
      device = "/dev/sda";
    };
  };

  fileSystems = [];


### config options
  nixpkgs.config = {
    # Configure the system python to have all bells and whistles
    python = {
      full = true;
      db4Support = true;
      opensslSupport = true;
      readlineSupport = true;
      sqliteSupport = true;
      tkSupport = true;
      # modules = [ "pymacs" ];
    };
  };

### Software, services
  services = {
    openssh.enable = true;
  };

  environment.systemPackages = with pkgs; [ 
    python27Packages.ipython python27Packages.lxml
    python27Packages.sphinx
    python27Packages.sqlite3 python27Packages.virtualenv pythonFull
    pythonPackages.readline
    # python27Packages.pylint     
    # pythonPackages.markdown pythonPackages.pymacs pythonPackages.rope
    # pythonPackages.ropemacs 
  ];

  i18n = {consoleKeyMap = "uk"; defaultLocale = "en_GB.UTF-8";};
  networking = {
    hostName = "python-test";
    extraHosts = ''
    '';
  };
  time.timeZone = "Europe/Berlin";
}

