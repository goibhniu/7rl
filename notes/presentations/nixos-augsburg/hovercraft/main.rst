:data-transition-duration: 2000
:skip-help: true

.. title: NixOS: The Purely Functional Linux Distribution

----

.. figure:: ../images/nixos-hires.png
   :width: 100%
           
   ..
   
   The Purely Functional Linux Distribution


----

.. figure:: ../images/thunderdome.jpeg
   :width: 100%

   ..

   ♫ We don't need another distro! ♪

.. note::
   To paraphrase Tina Turner

----

:data-x: r2000

.. image:: ../images/gldt1210.png
    :width: 2220px

----

:data-x: r-100
:data-scale: 11

.. note::
   The GNU/Linux Distribution Timeline, up to 2012
            
----

:data-y: -4500
:data-x: r800
:data-scale: 1.4

----

:data-y: 100

----

:data-x: 0
:data-y: r1000


.. image:: ../images/openhub-commits-pm.png
   :width: 100%

            
.. epigraph::
   
   NixOS has had 55,933 commits made by 438 contributors representing 479,972 lines of code
   
   -- openhub.net

----

:data-x: r0
:data-y: r1200

.. figure:: ../images/utrecht.jpg
   :width: 100%

   ..
   
   * Nix: A Safe and Policy-Free System for Software Deployment
   * http://nixos.org/~eelco/pubs/nspfssd-lisa2004-final.pdf (2004)
   * Eelco Dolstra, Merijn de Jonge, and Eelco Visser

----

.. figure:: ../images/tu-delft.jpg
   :width: 100%

   ..
   
   * The Purely Functional Software Deployment Model
   * https://nixos.org/~eelco/pubs/phd-thesis.pdf (2006)
   * Eelco Dolstra (niksnut)
         
----

=====================
 The NixOS Ecosystem
=====================

Nix
    Language + purely functional package manager
Nixpkgs
    Git repo containing the package expressions
NixOS
    Written in Nix, built by Nix
NixOps
    Tool for deploying NixOS systems
Hydra
    Continuous build system
  
----

==============
 Nix Features
==============

* Atomic upgrades ⚛
* Rollbacks
* Source + Binary
* Multi-user
* Development environments

----

================
 NixOS Features
================

All of the above plus:

* Declarative system configuration
* nixos-container
* NixOps for DevOps
  
----

========================
 Why do upgrades break?
========================

bin
   executable programs
lib
   shared libraries

No isolation. DLL/RPM hell. Be very careful!

.. note::
   No isolation between packages.
   Kid gloves
   Anyone use python? virtualenv
   Ruby? RVM
   Node? NVM
   VirtualMachines, Docker, Vagrant

----

:data-y: r1500

.. figure:: ../images/tech-foo-ubuntu-highlight_python.png
   :width: 100%
           
   ..
   
   It's complicated

----

====================
 Reliable or Fresh?
====================

Why not have both?

It's not you, it's the system!

.. note::
   Arch vs. Debian
   Backports, PPAs, Overlays
   Try submitting a bug report when something goes wrong!
   
   Stable distro ~= outdated packages, everybody solves the same
   issues until fixed globally.  

   Rolling release ~= things occasionally break on upgrade, takes
   effort to fix 

   "it's your job, you are the community" "WONTFIX"
   "It's your own fault", "Unsupported configuration", (rarely, but
   sometimes) you have to hunt around on forums just to fix an update.

   Ok, your system is always fixable, and it's fun. But, if you need a
   working system you will have to make some kind of compromise. I
   know there are Free Software devs running OsX just because of some
   silly breakage. 

   working computer = can do fun/cool stuff
   broken computer = can fix computer

   Search for hours through forums to fix a configuration

----

:data-rotate: 90

===================
 Purely Functional
===================

In computing, data structures or programming languages are called
**purely functional** if they guarantee the (weak) equivalence of
call-by-name, call-by-value and call-by-need evaluation strategies,
often by **excluding destructive modifications** (updates) of entities
in the program's running environment.

According to this restriction, variables are used in a mathematical
sense, with identifiers referring to **immutable, persistent values**.

.. note::

   Let's take a closer look

----

:data-x: r1500
         
=================
 For Development
=================

* Cherry pick dependencies
* nix-shell
* nixos-container
* Closures *nix-store --serve*
* Debugging

----

=================
 Nix Expressions
=================

.. code:: nix

   # nixpkgs/pkgs/top-level/all-packages.nix
   samplv1 = callPackage ../applications/audio/samplv1 { };

   # nixpkgs/pkgs/applications/audio/samplv1/default.nix
   { stdenv, fetchurl, jack2, libsndfile, lv2, qt4 }:

    stdenv.mkDerivation rec {
      name = "samplv1-${version}";
      version = "0.6.0";
    
      src = fetchurl {
        url = "mirror://sourceforge/samplv1/${name}.tar.gz";
        sha256 = "0fbkdb2dqw7v6n8j1h6pjz2019ylli9rmz8awywv2ycm8blr5pf0";
      };
    
      buildInputs = [ jack2 libsndfile lv2 qt4 ];
    }

----

=================
 Install samplv1
=================

.. code:: bash
          
   $ nix-env --install samplv1
   installing ‘samplv1-0.6.0’
   building path(s) ‘/nix/store/jcl8szkmwfp5vs1kpxacd7471ikfa6cy-user-environment’
   created 1120 symlinks in user environment
   
----

===========
 nix-shell
===========

.. code:: nix
          
    with import <nixpkgs> {}; {
      pyEnv = stdenv.mkDerivation {
        name = "py";
        buildInputs = with python34Packages; [
          docutils lxml python3 virtualenv
        ];
      };
    }

.. code:: bash
          
   $ nix-shell .

----

===========
 Isolation
===========

Profiles

* System
* User
* Development
   
----

=============
 Maintenance
=============

* nixos-rebuild switch --upgrade
* nix-collect-garbage --delete-old
* man configuration.nix
* nix-env --list-generations
* nix-env --rollback
* https://monitor.nixos.org
  
----

=======================
 Hydra: The Build Farm
=======================
 
* Binary applications (channels)
* Automatically builds and tests
* Single click install from url

----

==================
 Is NixOS For Me?
==================

* Can you edit text files?
* Can you fix syntax errors such as a missing ";"?
* KDE (4+5), GNOME (3.14), LibreOffice etc.
* Easy to write packages
  
----

===============
 Find Out More
===============

* https://nixos.org
* freenode #nixos
* http://lethalman.blogspot.de/search/label/nixpills
* Drop over to the stand!

----
   
===============
 The Nix Store
===============

Let's take a look!

.. note::
   which vlc
   readlink -f `which vlc`
   man ... vlc/share/man/man1/vlc.1.gz
   ldd ... vlc/bin/vlc
   ldd ... ffmpeg/bin/ffmpeg
   Each package is isolated, and only knows about its runtime dependencies

   each package has its own unique subdirectory:
   /nix/store/m1d9x7sh5b9275kh6cd36ww94a88rkbl-kdenlive-0.9.2

   the m1d9... is a unique identifier for the package that captures
   all its dependencies (it’s a cryptographic hash of the package’s
   build dependency graph). This enables many powerful features.

   You can have multiple versions or variants of a package installed
   at the same time. This is especially important when different
   applications have dependencies on different versions of the same
   package — it prevents the “DLL hell”. Because of the hashing
   scheme, different versions of a package end up in different paths
   in the Nix store, so they don’t interfere with each other. 

   An important consequence is that operations like upgrading or
   uninstalling an application cannot break other applications,
   since these operations never “destructively” update or delete
   files that are used by other packages.

   Nix helps you make sure that package dependency specifications
   are complete. In general, when you’re making a package for a
   package management system like RPM, you have to specify for each
   package what its dependencies are, but there are no guarantees
   that this specification is complete. If you forget a dependency,
   then the package will build and work correctly on your machine if
   you have the dependency installed, but not on the end user’s
   machine if it’s not there.  

   Since Nix doesn’t install packages in “global” locations like
   /usr/bin there is no risk of incomplete dependencies. If the
   dependencies aren't specified in the package expression they will
   not be found. There is no need to test the package in a chroot,
   or in a VM.
   
   



